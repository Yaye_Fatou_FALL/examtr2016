package org.FALL.exo1;

import java.util.Random;

public class Compte implements Comparable<Compte> {
	private int ID;
	private Client client;
	private double solde;
	
	public Compte(){};
	
	public Compte(Client client, int solde){
		this.ID=new Random().nextInt();
		this.client=client;
		this.solde=solde;
		
	}

	public String toString() {
		return "Compte [ID compte=" + ID + ", client=" + client + ", solde=" + solde + "]";
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(int solde) {
		this.solde = solde;
	}
	
	public void crediter(double montant){
		solde=solde+montant;
		
	}
	
	public void debiter(double montant){
		if((solde-montant) >=-100)
		solde =solde-montant;
		
	}
	
	public int compareTo(Compte compte) {
		
		
		if (this.client.getNom().equals(compte.client.getNom()))
			return this.client.getPrenom().compareTo(compte.client.getPrenom());
		else
			return this.client.getNom().compareTo(compte.client.getNom());
	
	}

}
