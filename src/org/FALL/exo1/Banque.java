package org.FALL.exo1;

import java.util.SortedSet;
import java.util.TreeSet;

public class Banque   {

	SortedSet <Compte> setCompte= new TreeSet <Compte>(); 

	public Banque(){}


	public void creerCompte(Client client ,int soldeini){

		Compte compte=new Compte(client,soldeini);

	}
	public void afficherCompte(){
		for(Compte compte: setCompte)
			System.out.println(compte);
	}

	public Compte rechercherCompte(String nom){
		for(Compte compte: setCompte){
			if(compte.getClient().getNom().equals(nom))
				return compte;
		}
		return null;
	}

	public Compte afficherCompte(int ID){
		for(Compte compte: setCompte){
			if(compte.getClient().getID()==ID)
				return compte;
		}
		return null;
	}


	public  void crediterCompte(int ID,double montant){
		for(Compte compte: setCompte){
			if(compte.getClient().getID()==ID){
				compte.crediter(montant);
			}
		}

	}

	public void debiterCompte (int ID,double montant){
		for(Compte compte: setCompte){
			if(compte.getClient().getID()==ID){
				compte.debiter(montant);
			}
		}

	}

	public void SupprimerCompte (int ID){
		for(Compte compte: setCompte){
			if(compte.getClient().getID()==ID)
				setCompte.remove(compte);
		}

	}

	public void Virement (int ID1,int ID2,double montant){
		for(Compte compte: setCompte){
			if(compte.getClient().getID()==ID1){
				debiterCompte(ID1,montant);
				crediterCompte(ID2,montant);
			}
		}
	}


	public SortedSet<Compte> getSetCompte() {
		return setCompte;
	}


	
}






