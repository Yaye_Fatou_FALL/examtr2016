package org.FALL.exo1;

import java.util.Random;

public class Client {
	private   int ID;
	private String nom;
	private String prenom;
	private int anneeNaissance;
	private String adresse;
	private String tel;
	
	private  Random random = new Random() ;

	/**
	 * 
	 */
	public Client(){}

	/**
	 * 
	 * @param ID
	 * @param nom
	 * @param prenom
	 * @param anneeNaissance
	 * @param adresse
	 * @param tel
	 */
	public Client(String prenom,String nom,int anneeNaissance ,String adresse, String tel){
		
		this.ID=random.nextInt() ;
		this.nom=nom;
		this.prenom=prenom;
		this.anneeNaissance=anneeNaissance;
		this.adresse=adresse;
		this.tel=tel;
		
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getAnneeNaissance() {
		return anneeNaissance;
	}

	public void setAnneeNaissance(int anneeNaissance) {
		this.anneeNaissance = anneeNaissance;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	@Override
	public String toString() {
		return "Client [ID=" + ID + ", nom=" + nom + ", prenom=" + prenom + ", anneeNaissance=" + anneeNaissance
				+ ", adresse=" + adresse + ", tel=" + tel + "]";
	}

}